var express = require("express");
var bodyParser = require("body-parser");
var app = express();
var cors = require("cors");

// var requests = require("./requests");

app.use(bodyParser.json({}));
app.use(cors());
const axios = require("axios");
var port = process.env.PORT || 3000;
app.listen(port, function() {
  console.log("Listening on port ", port, "...");
});

let endpoint = "https://ws.quantatel.com/ikhair/causes?country=AE";

app.get("/mock", async (req, res) => {
  // console.log(req.body.results);
  axios
    .get(endpoint)
    .then(function(response) {
      // handle success
      return res.json(response.data);
    })
    .catch(function(error) {
      // handle error
      console.log(error);
      res.sendStatus(500);
    });
});
